//
// Created by Lukáš Stankovič on 07.05.17.
//

#ifndef TESTY_NA_ZKOUSKU_VAZBY_H
#define TESTY_NA_ZKOUSKU_VAZBY_H

#include <iostream>

class Predek {
public:
	Predek();
	void casnaVazba();
	virtual void pozdniVazba();
};

class Potomek : public Predek {
public:
	Potomek();
	void casnaVazba();
	void pozdniVazba();
};

class DruhyPotomek : public Predek {
public:
	DruhyPotomek();
	void casnaVazba();
	void pozdniVazba();
};

class PotomekPotomka : public Potomek {
public:
	PotomekPotomka();
	void casnaVazba();
	void pozdniVazba();
};
#endif //TESTY_NA_ZKOUSKU_VAZBY_H
