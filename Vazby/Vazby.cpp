//
// Created by Lukáš Stankovič on 07.05.17.
//

#include "Vazby.h"

void Predek::casnaVazba() {
	std::cout << "Předek" << std::endl;
}

void Predek::pozdniVazba() {
	std::cout << "Předek" << std::endl;
}

Predek::Predek() {
	std::cout << "Konstrokutor předka" << std::endl;
}

void Potomek::casnaVazba() {
	std::cout << "Potomek" << std::endl;
}

void Potomek::pozdniVazba() {
	std::cout << "Potomek" << std::endl;
}

Potomek::Potomek() {
	std::cout << "Konstrokutor potomka" << std::endl;
}

void DruhyPotomek::casnaVazba() {
	std::cout << "Druhý potomek" << std::endl;
}

void DruhyPotomek::pozdniVazba() {
	std::cout << "Druhý potomek" << std::endl;
}

DruhyPotomek::DruhyPotomek() {
	std::cout << "Konstrokutor druhého potomka" << std::endl;
}

void PotomekPotomka::casnaVazba() {
	std::cout << "Potomek potomka" << std::endl;
}

void PotomekPotomka::pozdniVazba() {
	std::cout << "Potomek potomka" << std::endl;
}

PotomekPotomka::PotomekPotomka() {
	std::cout << "Konstrokutor potmka potomka" << std::endl;
}
