//
// Created by Lukáš Stankovič on 05.05.17.
//

#ifndef TESTY_NA_ZKOUSKU_RECTANGLE_H
#define TESTY_NA_ZKOUSKU_RECTANGLE_H

#include "../ShapeInterface/ShapeInterface.h"

class Rectangle : public ShapeInterface{
private:
	/**
	 * Width of the rectangle
	 */
	double width;

	/**
	 * Height of the rectangle
	 */
	double height;
public:

	/**
	 * Width setter
	 * @param width
	 */
	void setWidth(double width);

	/**
	 * Height setter
	 * @param height
	 */
	void setHeight(double height);

	/**
	 * Area of the rectangle
	 * @return double
	 * */
	double getArea();

	/**
	 * Who am I
	 */
	void whoAmI();
};


#endif //TESTY_NA_ZKOUSKU_RECTANGLE_H
