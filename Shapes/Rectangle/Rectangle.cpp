//
// Created by Lukáš Stankovič on 05.05.17.
//

#include "Rectangle.h"

double Rectangle::getArea() {
	return this->width * this->height;
}

void Rectangle::setWidth(double width) {
	this->width = width;
}

void Rectangle::setHeight(double height) {
	this->height = height;
}

void Rectangle::whoAmI() {
	std::cout << "I am rectangle!" << std::endl;
}
