//
// Created by Lukáš Stankovič on 05.05.17.
//

#include "Triangle.h"

double Triangle::getArea() {
	return (this->width * this->height) / 2;
}

void Triangle::setWidth(double width) {
	this->width = width;
}

void Triangle::setHeight(double height) {
	this->height = height;
}

void Triangle::whoAmI() {
	std::cout << "I am triangle!" << std::endl;
}
