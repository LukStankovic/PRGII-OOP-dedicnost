//
// Created by Lukáš Stankovič on 05.05.17.
//

#ifndef TESTY_NA_ZKOUSKU_TRIANGLE_H
#define TESTY_NA_ZKOUSKU_TRIANGLE_H

#include <zconf.h>
#include "../ShapeInterface/ShapeInterface.h"

class Triangle : public ShapeInterface{
private:
	/**
	 * Width of the triangle
	 */
	double width;

	/**
	 * Height of the Triangle
	 */
	double height;
public:

	/**
	 * Width setter
	 * @param width
	 */
	void setWidth(double width);

	/**
	 * Height setter
	 * @param height
	 */
	void setHeight(double height);

	/**
	 * Area of the triangle
	 * @return double
	 * */
	double getArea();

	/**
	 * Who am I
	 */
	void whoAmI();
};


#endif //TESTY_NA_ZKOUSKU_TRIANGLE_H
