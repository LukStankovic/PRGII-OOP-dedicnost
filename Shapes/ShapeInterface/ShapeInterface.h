//
// Created by Lukáš Stankovič on 05.05.17.
//

#ifndef TESTY_NA_ZKOUSKU_SHAPE_H
#define TESTY_NA_ZKOUSKU_SHAPE_H

#include <iostream>

/**
 * @brief Třída je čistě virtuální - abstraktní
 * Je tedy jako interface v ostatních jazycích
 */

class ShapeInterface {
public:
	/**
	 * Width setter
	 * @param width
	 */
	virtual void setWidth(double width) = 0;

	/**
	 * Height setter
	 * @param height
	 */
	virtual void setHeight(double height) = 0;

	/**
	 * Virtual method for area
	 * @return double
	 */
	virtual double getArea() = 0;

	/**
	 * Who am I
	 */
	virtual void whoAmI() = 0;
};


#endif //TESTY_NA_ZKOUSKU_SHAPE_H
