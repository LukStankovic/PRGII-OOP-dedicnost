#include <iostream>

#include "Vehicles/Vehicle/Vehicle.h"
#include "Vehicles/TirTruck/TirTruck.h"
#include "Vehicles/Car/Car.h"

#include "Shapes/Rectangle/Rectangle.h"
#include "Shapes/Triangle/Triangle.h"

#include "Vazby/Vazby.h"

#include "Template/Box.h"

using namespace std;

int main() {

	cout << "-----------" << endl;
	cout << "Test dedeni" << endl;
	cout << "-----------" << endl << endl;

	Vehicle *octavia = new Car(4, 1300, "red", "1T1 4212", "Škoda", 140);
	Vehicle *rapid = new Car(4, 1240, "blue", "8T2 2142", "Škoda", 120);
	Vehicle *mondeo = new Car(4, 1560, "black", "8A2 3245", "Ford", 60);

	cout << "Počet vozidel: " << Vehicle::getVehiclesCount() << endl;

	delete rapid;

	cout << endl << "Počet vozidel: " << Vehicle::getVehiclesCount() << endl;

	Vehicle *scania = new TirTruck(8, 15600, "green", "1A2 1349", "Scania", 5000, false);

	cout << "Počet vozidel: " << Vehicle::getVehiclesCount() << endl << endl;


	cout << "Dojezd Octavia: " << octavia->range() << " km" << endl;
	cout << "Dojezd Mondeo: " << mondeo->range() << " km" << endl;
	cout << "Dojezd Scania: " << scania->range() << " km" << endl;

	delete octavia;
	delete mondeo;
	delete scania;

	cout << "Počet vozidel: " << Vehicle::getVehiclesCount() << endl << endl;

	cout << "---------------" << endl;
	cout << "Test interface" << endl;
	cout << "---------------" << endl << endl;
	ShapeInterface *triangle = new Triangle;
	ShapeInterface *rectangle = new Rectangle;

	triangle->setHeight(3);
	triangle->setWidth(6);

	rectangle->setHeight(3);
	rectangle->setWidth(6);

	triangle->whoAmI();
	rectangle->whoAmI();

	cout << "Obsah trojuhelnika: " << triangle->getArea() << endl;
	cout << "Obsah obdelnika: " << rectangle->getArea() << endl << endl;

	delete triangle;
	delete rectangle;


	cout << "----------" << endl;
	cout << "Test vazeb" << endl;
	cout << "----------" << endl << endl;

	Predek *a = new Potomek();

	cout << "Vypíše se předek, protože u funkce casnaVazba chybí virtual: ";
	a->casnaVazba();

	cout << "Vypíše se potomek, protože funkce pozdniVazba u predka má virtual: ";
	a->pozdniVazba();


	cout << endl;

	Predek *b = new DruhyPotomek();

	cout << "Vypíše se předek, protože u funkce casnaVazba chybí virtual: ";
	b->casnaVazba();

	cout << "Vypíše se druhý potomek, protože funkce pozdniVazba u predka má virtual: ";
	b->pozdniVazba();

	cout << endl;

	Predek *c = new PotomekPotomka();

	c->casnaVazba();
	c->pozdniVazba();


	delete a;
	delete b;
	delete c;

	cout << "-------------" << endl;
	cout << "Test templatů" << endl;
	cout << "-------------" << endl << endl;


	A *intNumber = new A(10);
	S *str = new S("Hello world!");

	Box<A> *tIntNumber = new Box<A>(intNumber);
	Box<S> *tStr = new Box<S>(str);

	cout << "A: " << tIntNumber->getInstance()->getValue() << endl;
	cout << "S: " << tStr->getInstance()->getValue() << endl;

	return 0;
}