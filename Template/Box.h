//
// Created by Lukáš Stankovič on 07.05.17.
//

#ifndef TESTY_NA_ZKOUSKU_BOX_H
#define TESTY_NA_ZKOUSKU_BOX_H

#include <iostream>

template<class T>
class Box {
private:
	T *instance;

public:
	Box(T *instance);
	T *getInstance();
};

template<class T>
Box<T>::Box(T *instance) {
	this->instance = instance;
}

template<class T>
T *Box<T>::getInstance() {
	return this->instance;
}



class A {
private:
	int value;

public:
	A(int v);
	int getValue();
};


class S {
private:
	std::string value;

public:
	S(std::string v);
	std::string getValue();

};
#endif //TESTY_NA_ZKOUSKU_BOX_H
