//
// Created by Lukáš Stankovič on 07.05.17.
//

#include "Box.h"


A::A(int v) {
	this->value = v;
}

int A::getValue() {
	return this->value;
}

S::S(std::string v) {
	this->value = v;
}

std::string S::getValue() {
	return this->value;
}
