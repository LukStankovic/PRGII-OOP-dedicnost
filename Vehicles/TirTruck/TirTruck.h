//
// Created by Lukáš Stankovič on 05.05.17.
//

#ifndef TESTY_NA_ZKOUSKU_TIRTRUCK_H
#define TESTY_NA_ZKOUSKU_TIRTRUCK_H

#include "../Vehicle/Vehicle.h"

class TirTruck : public Vehicle {
private:
	/**
	 * Truck`s capacity
	 */
	double loadCapacity;

	/**
	 * True -> loadCapacity of truck is used to calculate the range
	 */
	bool trailer;

public:
	/**
	 * TirTruck Constructor
	 * @param numWheels
	 * @param height
	 * @param colour
	 * @param spz
	 * @param manufacturer
	 * @param loadCapacity
	 * @param trailer
	 */
	TirTruck(int numWheels, double weight, string colour, string spz, string manufacturer, double loadCapacity, bool trailer);

	/**
    * TirTruck destructor
    */
	~TirTruck();


	/**
	 * Range for the truck with static Vehicle::TIR_COEFFICIENT constant
	 * @return double Range
	 */
	virtual double range();
};


#endif //TESTY_NA_ZKOUSKU_TIRTRUCK_H
