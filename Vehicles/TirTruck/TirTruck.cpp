//
// Created by Lukáš Stankovič on 05.05.17.
//

#include "TirTruck.h"

TirTruck::TirTruck(int numWheels, double weight, string colour, string spz, string manufacturer, double loadCapacity, bool trailer) : Vehicle(numWheels, weight, colour, spz, manufacturer) {
	this->loadCapacity = loadCapacity;
	this->trailer = trailer;
}

TirTruck::~TirTruck() {
	cout << endl << "TirTruck destructor" << endl;
}

double TirTruck::range() {
	return (this->getWeight() - (this->trailer ? this->loadCapacity : 0)) / Vehicle::TIR_COEFFICIENT;
}
