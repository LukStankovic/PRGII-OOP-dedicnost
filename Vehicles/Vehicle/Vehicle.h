//
// Created by Lukáš Stankovič on 05.05.17.
//

#ifndef TESTY_NA_ZKOUSKU_VEHICLE_H
#define TESTY_NA_ZKOUSKU_VEHICLE_H

#include <iostream>

using namespace std;

/**
 * Parrent class for vehicles
 */
class Vehicle {
private:
	/**
	 * Static vehicle counter
	 */
	static int vehiclesCount;

	/**
	 * Number of wheels of the car
	 */
	int numWheels;

	/**
	 * Weight of the car
	 */
	double weight;

	/**
	 * Colour of the car
	 */
	string colour;

	/**
	 * Vehicle registration plate of the car
	 */
	string spz;

	/**
	 * Manufacturer of the car
	 */
	string manufacturer;

public:
	/**
	 * Get static counter
	 * @return int Vehicles counter
	 */
	static int getVehiclesCount();

	/**
    * Car coefficient used to calculate range
    */
	static double CAR_COEFFICIENT;

	/**
    * Truck coefficient used to calculate range
    */
	static double TIR_COEFFICIENT;

	/**
	 * Parrent vehicle constructor
	 * @param numWheels
	 * @param height
	 * @param colour
	 * @param spz
	 * @param manufacturer
	 */
	Vehicle(int numWheels, double weight, string colour, string spz, string manufacturer);

	/**
	 * Parrent vehicle destructor
	 */
	virtual ~Vehicle();

	/**
	 * Range
	 * @return double
	 */
	virtual double range() = 0;

	/**
	 * Number of wheels
	 * @return int number of wheels
	 */
	int getNumWheels() const;

	/**
	 * Colour of the vehicle
	 * @return string
	 */
	const string &getColour() const;

	/**
	 * Vehicle registration plate of the car
	 * @return string
	 */
	const string &getSpz() const;

	/**
	 * Manufacturer of the car
	 * @return string
	 */
	const string &getManufacturer() const;

	/**
	 * Weight of the car
	 * @return double Weight
	 */
	double getWeight() const;
};


#endif //TESTY_NA_ZKOUSKU_VEHICLE_H
