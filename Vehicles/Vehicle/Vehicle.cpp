//
// Created by Lukáš Stankovič on 05.05.17.
//

#include "Vehicle.h"

int Vehicle::vehiclesCount = 0;

double Vehicle::CAR_COEFFICIENT = 1.6;

double Vehicle::TIR_COEFFICIENT = 5.5;

Vehicle::Vehicle(int numWheels, double weight, string colour, string spz, string manufacturer) {
	this->numWheels = numWheels;
	this->weight = weight;
	this->colour = colour;
	this->spz = spz;
	this->manufacturer = manufacturer;
	Vehicle::vehiclesCount++;
}

Vehicle::~Vehicle() {
	Vehicle::vehiclesCount--;
	cout << endl << "Vehicle destructor: " << this->spz << " -> " << this->manufacturer << endl;
}

int Vehicle::getVehiclesCount() {
	return Vehicle::vehiclesCount;
}

int Vehicle::getNumWheels() const {
	return numWheels;
}

const string &Vehicle::getColour() const {
	return colour;
}

const string &Vehicle::getSpz() const {
	return spz;
}

const string &Vehicle::getManufacturer() const {
	return manufacturer;
}

double Vehicle::getWeight() const {
	return weight;
}

