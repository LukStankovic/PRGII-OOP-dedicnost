//
// Created by Lukáš Stankovič on 05.05.17.
//

#ifndef TESTY_NA_ZKOUSKU_CAR_H
#define TESTY_NA_ZKOUSKU_CAR_H

#include "../Vehicle/Vehicle.h"

class Car : public Vehicle {
private:
	/**
	 * Car`s trunk volume
	 */
	int trunkVolume;

public:
	/**
	 * Car constructor
	 * @param numWheels
	 * @param height
	 * @param colour
	 * @param spz
	 * @param manufacturer
	 * @param trunkVolume
	 */
	Car(int countWheel, double weight, string colour, string spz, string manufacturer, int trunkVolume);

	/**
	 * Car destructor
	 */
	~Car();

	/**
	 * Range for the car with static Vehicle::CAR_COEFFICIENT constant
	 * @return double Range
	 */
	virtual double range();
};


#endif //TESTY_NA_ZKOUSKU_CAR_H
