//
// Created by Lukáš Stankovič on 05.05.17.
//

#include "Car.h"

Car::Car(int numWheels, double weight, string colour, string spz, string manufacturer, int trunkVolume) : Vehicle(numWheels, weight, colour, spz, manufacturer){
	this->trunkVolume = trunkVolume;
}

Car::~Car() {
	cout << endl << "Car destructor: " << this->getSpz() << " -> " << this->getManufacturer() << endl;
}

double Car::range() {
	return (this->getWeight()) / Vehicle::CAR_COEFFICIENT;
}
